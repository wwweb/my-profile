# Hello!

My name is Vladimir Skriabin.  
I'm a Backend Software Developer.  

Considering only remote job positions.

![portrait photo](me240.jpg)


## Summary

Born in Yakutsk (Russia) on May 21, 1986.  
Current position: Freelancer, full-stack web development.  
Currently living in Hua Hin, Thailand.  
Single, no children.

Speaking languages:

* Russian - native
* English - [ILR level 2](https://en.wikipedia.org/wiki/ILR_scale#ILR_Level_2_.E2.80.93_Limited_working_proficiency)
* Thai - basic speaking

Professional skills

* 10+ years of professional web software development, mostly in E-Commerce projects.
* Main programming languages: PHP, JavaScript.
* Proficient in PHP backend development and web technologies in general.
* Extensive skills in API integrations and parsers, collecting data from different sources.
* Managing RDBMS such as, MySQL / MariaDB / PostgreSQL / SQLite. Writing complex SQL queries.  
  Familiar with NoSQL DBMS, such as MongoDB and CouchDB.
* Experienced with Linux servers administration (Ubuntu/Debian, CentOS).  
  Configuring and managing web servers, databases and related software.  
  Setting up server environment, configuring daemons, cron schedules etc.
* Good skills in native JavaScript, both frontend and backend (NodeJS).  
  Familiar with popular libraries and frameworks.
* Good understanding of Search Engine Optimization technologies,  
  especially within E-Commerce projects.
* Able to follow coding style and code fomatting rules.
* Not so good in drawing or designing, but I'm good at writing proper HTML and CSS,  
  creating responsive layouts and SEO-optimized HTML-markup.

<div style="page-break-after: always; visibility: hidden"></div>

## Career

In 2008-2010 I just started learning programming and made a few small projects for friends and myself.  

In 2010-2012 worked as a freelancer making websites for small local companies in my hometown.  
Created and promoted a few e-commerce websites.

In 2012 got employed by [Sinet Group Ltd.](https://sinet.team/)  
It was my first experience in a big IT company.  
As a junior developer I was working on a new E-Commerce project for local market.  
Also got some experience in another project, developing a centralized comment managing service  
for company's projects.

In 2013 moved to another company - [EPL Diamond](https://epldiamond.com).  
Developed a specialized E-Commerce platform and online store.  
This also includes the development of backend dashboard with analytics and reporting tools  
and integration with company's accounting system.  
Got some experience in providing technical support for business and customers.

In 2014 joined a new startup project as a business partner - [Yamibox.com](https://yamibox.com).  
Developed a specialized E-Commerce platform and online store.  
Since then I relocated to Thailand and worked remotely.

From 2016 to 2021 worked at [12go.asia](https://12go.asia).  
Main responsibilities: integrations of API / services provided by business partners;  
code reviews, refactoring and optimization of existing code base;  
server-side optimizations - SQL queries, web server and database configurations, etc.  
Later on, as a team lead, more often I had to deal with code reviews, team management  
and business workflow.

In 2022 worked at [Shining Light Co. Ltd.](https://www.shininglight-piercing.com/).  
Provided technical support and improvements to one of company's projects, such as:  
fixes and optimization in backend and frontend, code refactoring and cleanup,  
server environment re-configuration, SEO optimization etc.

Since 2022 I work as a freelancer and open for offers.


## Contact me

* Email: [wwweb.sinet@gmail.com](mailto:wwweb.sinet@gmail.com)
* Telegram: [Vladimir Skriabin](https://t.me/bobo_huahin)
* Phone: +66942514020 (Thailand)
